<?
class BBClient{
	protected $bbtoken = '11570.pbpqebfc';
	protected $api_url = 'http://api.boxberry.de/json.php';
	
	protected function getBaseApiUrl($params){
		$str="";
		if(count($params>0)){
			//если переданы параметры - формируем урл для доступа к апи ББ
			foreach($params as $k=>$v){
				
				$str.="&".$k."=".$v;
			}
		}
		return $this->api_url."?token=".$this->bbtoken.$str;
	}
	
	protected function apiCall($params){
		try{		
			$handle = fopen($this->getBaseApiUrl($params), "rb");
			$contents = stream_get_contents($handle);
			fclose($handle);
			$data=json_decode($contents,true);
			if(count($data)<=0 or $data[0]['err'])
			{ 
				throw new Exception("Ошибка запроса к апи ББ ".$data[0]['err']);
				
			}
			else
			{
				//выводим json (немного коряво, т.к приходится его 2 раза разбирать)
				return $contents;

			}
				
		}catch(Exception $e){
			throw new Exception("Внутренняя ошибка, возможно передан не верный список параметров  ".$e->getMessage());
		}
		
	}
	
	public function getCities(){
		return $this->apiCall(array('method'=>'ListCities'));
	}
	public function getDeliveryCost($city_code,$box_weight,$box_price){
		//валидация
		if(!preg_match("/\d+/", $city_code)){
			throw new Exception("Не корректно указан код города доставки");
		}
		$box_weight = (float)$box_weight;
		$box_price = (float)$box_price;
		$params=array(
			'method'=>'DeliveryCosts',
			'weight'=>$box_weight,
			'target'=>$city_code,
			'ordersum'=>$box_price
			);
			
		return $this->apiCall($params);

	}
}

?>